import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.junit.Assert;



public class PunterTest {
	
	private Die d1 = Mockito.mock(Die.class);
	private Die d2 = Mockito.mock(Die.class);
	private Die d3 = Mockito.mock(Die.class);
	
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		Mockito.when(d1.getFace()).thenReturn(Face.CRAB);
		Mockito.when(d2.getFace()).thenReturn(Face.FISH);
		Mockito.when(d3.getFace()).thenReturn(Face.STAG);
		
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void loseBetFail() {
		
		Punter player =new Punter ("Ran");
		Round.play(player, Arrays.asList(d1,d2,d3),Face.PRAWN, 20);
		
		Assert.assertEquals(80, player.getBalance ());
		
	}

}
