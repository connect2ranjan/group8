import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.junit.Assert;

public class facetest {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
	Punter player = new Punter("Ran", 1000,10);
	Die s1 = new Die();
	Die s2 = new Die();
	Die s3 = new Die();
	int totalGames =0;
	int wins =0;
	while (true) {
		try {
			int winningAmount =Round.play(player, Arrays.asList(s1,s2,s3),Face.FISH,10);
			if (winningAmount >0) {
				wins++;
			}
			totalGames++;
		} catch(IllegalArgumentException iae) {
			break;
		}
		
		}
	double ratio = (double) wins / (double) totalGames;
	Assert.assertEquals(0.42, ratio, 0.01);
					
		}
	}

	

